import 'package:flutter/material.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/services/profile_api.dart';
import 'package:security_company_app/app/common/models/client.dart' as cl;

class ProfileProvider with ChangeNotifier {
  String firstName;
  String laseName;
  String phone;
  String currentpassword;
  String newPassword;
  ProfileApi _profileApi = ProfileApi();
  bool isLoading = false;

  void notify() => notifyListeners();

  ProfileProvider() {
    print("yesss");

    getProfile();
  }

  getProfile() async {
    final result = await _profileApi.get_user_infos();
    print("------");

    print(result);

    firstName = result.firstName as String;
    laseName = result.lastName as String;
    phone = result.phoneNumber as String;

    print(firstName);

    notify();

    return result;
  }

  Future<bool> updateProfile() async {
    notify();
    isLoading = true;
    final result = await _profileApi.updateProfile(
        currentPassword: currentpassword,
        newPassword: newPassword,
        firstName: firstName,
        lastName: laseName,
        phone: phone);
    isLoading = false;
    notify();
    return result;
  }
}
