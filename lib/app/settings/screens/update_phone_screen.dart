import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/provider/profile_provider.dart';

const Color mainColor = Color(0xff212A37);

class UpdatePhoneScreen extends StatefulWidget {
  @override
  _UpdatePhoneScreenState createState() => _UpdatePhoneScreenState();
}

class _UpdatePhoneScreenState extends State<UpdatePhoneScreen> {
  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }

   TextEditingController phoneController = TextEditingController();


   @override
  void initState() {
    super.initState();
   WidgetsBinding.instance.addPostFrameCallback(initPhone);
  }

  void initPhone(_){
    final provider = Provider.of<ProfileProvider>(context,listen: false);
    phoneController.text = provider.phone;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffE3E9F0),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: const Color(0xffE3E9F0),
            image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topRight,
        )),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: screenUtilStp(90)),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setSp(20).toDouble()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: ScreenUtil().setSp(46).toDouble(),
                ),
                buildHeader(context),
                _buildTitleHeader(),
                _buildInputs(),
                SecPrimaryButton(
                  color: Color(0xFF4361BA),
                  text: 'Save',
                  isLoading: false,
                  onTap: () {
                    final profileProvider = Provider.of<ProfileProvider>(context,listen: false);
                    profileProvider.phone = phoneController.text;
                    profileProvider.updateProfile();

                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }


  Widget _buildInputs() {
    return Container(
      margin:
          EdgeInsets.only(top: screenUtilStp(30), bottom: screenUtilStp(30)),
      padding: EdgeInsets.symmetric(
          vertical: screenUtilStp(40), horizontal: screenUtilStp(30)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(screenUtilStp(20)),
      ),
      child: _buildInput(
        controller: phoneController,
        label: 'Phone number'
      ),
    );
  }

  Widget _buildInput({TextEditingController controller, String label}) {
    return TextField(
      keyboardType: TextInputType.phone,
      controller: controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(
          bottom: ScreenUtil().setHeight(30.5).toDouble(),
        ),
        hintText: label,
        hintStyle: TextStyle(
          fontFamily: 'Rubik',
          fontSize: ScreenUtil().setSp(14).toDouble(),
          color: Color(0xFFB8BCC7),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
      ),
    );
  }

  Widget _buildTitleHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: screenUtilStp(50),
        ),
        Text(
          'Phone number',
          style: TextStyle(
            fontSize: screenUtilStp(23),
            fontWeight: FontWeight.w500,
            color: Color(0xff363E49),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 9),
          height: 3,
          width: 30,
          decoration: BoxDecoration(
            color: Color(0xff363E49),
            borderRadius: BorderRadius.circular(6),
          ),
        ),
        SizedBox(
          height: screenUtilStp(18),
        ),
        Text(
          'This makes it easier for everyone to be reachable if any issue happens.',
          style:
              TextStyle(color: Color(0xff888792), fontSize: screenUtilStp(12)),
        ),
      ],
    );
  }

  Widget buildHeader(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: CircleAvatar(
            radius: ScreenUtil().setSp(27).toDouble(),
            backgroundColor: Colors.white,
            child: Icon(
              Icons.arrow_back,
              color: mainColor,
            ),
          ),
        ),
        Text(
          'SETTINGS',
          style: TextStyle(
              fontSize: ScreenUtil().setSp(17).toDouble(),
              color: Color(0xff4361BA),
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
