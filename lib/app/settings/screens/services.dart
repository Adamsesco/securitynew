import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class Services extends StatefulWidget {
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        return Scaffold(
          body: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                color: const Color(0xffE3E9F0),
                image: DecorationImage(
                  image: AssetImage('assets/images/background.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.topRight,
                )),
            child: SingleChildScrollView(
              padding: EdgeInsets.only(bottom: screenUtilStp(90)),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setSp(20).toDouble(),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: ScreenUtil().setSp(46).toDouble(),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => Navigator.of(context).pop(),
                          child: CircleAvatar(
                            radius: ScreenUtil().setSp(27).toDouble(),
                            backgroundColor: Colors.white,
                            child: Icon(
                              Icons.arrow_back,
                              color: Color(0xff212A37),
                            ),
                          ),
                        ),
                        Text(
                          'SETTINGS',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(17).toDouble(),
                              color: Color(0xff4361BA),
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    //appbar end
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: screenUtilStp(50),
                        ),
                        Text(
                          'Business Services',
                          style: TextStyle(
                            fontSize: screenUtilStp(23),
                            fontWeight: FontWeight.w500,
                            color: Color(0xff363E49),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 9),
                          height: 3,
                          width: 30,
                          decoration: BoxDecoration(
                            color: Color(0xff363E49),
                            borderRadius: BorderRadius.circular(6),
                          ),
                        ),
                        SizedBox(
                          height: screenUtilStp(18),
                        ),
                        Text(
                          'Please choose the services that your company may be able to serve.',
                          style: TextStyle(
                              color: Color(0xff888792),
                              fontSize: screenUtilStp(12)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenUtilStp(21),
                    ),

                    Container(
                      height: 110,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Color(0xffFBF9F7),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
