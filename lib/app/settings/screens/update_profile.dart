import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/security_app/bloc/app_bloc.dart';
import 'package:security_company_app/app/security_app/bloc/app_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

String role;

class _UpdateProfileState extends State<UpdateProfile> {
  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }
  /*

  getRole() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    role = prefs.getString('role');
  }

  @override
  void initState() {
    getRole();
    super.initState();
  }
  */

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        final theme = getThemeFromState(state);
        return Scaffold(
          //provider type agent profile view ..... indiv to be implemented later
          body: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                color: const Color(0xffE3E9F0),
                image: DecorationImage(
                  image: AssetImage('assets/images/background.png'),
                  fit: BoxFit.cover,
                  alignment: Alignment.topRight,
                )),
            child: SingleChildScrollView(
              padding: EdgeInsets.only(bottom: screenUtilStp(90)),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setSp(20).toDouble(),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: ScreenUtil().setSp(46).toDouble(),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => Navigator.of(context).pop(),
                          child: CircleAvatar(
                            radius: ScreenUtil().setSp(27).toDouble(),
                            backgroundColor: Colors.white,
                            child: Icon(
                              Icons.arrow_back,
                              color: Color(0xff212A37),
                            ),
                          ),
                        ),
                        Text(
                          'SETTINGS',
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(17).toDouble(),
                              color: Color(0xff4361BA),
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    //appbar end
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: screenUtilStp(50),
                        ),
                        Text(
                          'Profile',
                          style: TextStyle(
                            fontSize: screenUtilStp(23),
                            fontWeight: FontWeight.w500,
                            color: Color(0xff363E49),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 9),
                          height: 3,
                          width: 30,
                          decoration: BoxDecoration(
                            color: Color(0xff363E49),
                            borderRadius: BorderRadius.circular(6),
                          ),
                        ),
                        SizedBox(
                          height: screenUtilStp(18),
                        ),
                        Text(
                          'Primum igitur, inquit, sic agam, ut ipsi auctori huius disciplinae placet: constituam, quid.',
                          style: TextStyle(
                              color: Color(0xff888792),
                              fontSize: screenUtilStp(12)),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Container(
                      margin: EdgeInsets.only(
                          top: screenUtilStp(30), bottom: screenUtilStp(30)),
                      padding: EdgeInsets.symmetric(
                          vertical: screenUtilStp(40),
                          horizontal: screenUtilStp(30)),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(screenUtilStp(20)),
                      ),
                      child: Form(
                        child: Column(
                          children: [
                            CustomInput(label: 'Fisrt Name'),
                            SizedBox(height: screenUtilStp(33)),
                            CustomInput(label: 'Last Name'),
                            SizedBox(height: screenUtilStp(33)),
                            CustomInput(label: 'Phone Number'),
                            SizedBox(height: screenUtilStp(60)),
                            TextFormField(
                              maxLines: 5,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  bottom:
                                      ScreenUtil().setHeight(30.5).toDouble(),
                                ),
                                hintText: 'Description',
                                hintStyle: TextStyle(
                                  fontFamily: 'Rubik',
                                  fontSize: ScreenUtil().setSp(14).toDouble(),
                                  color: Color(0xFFB8BCC7),
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFFD8D8D8),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFFD8D8D8),
                                  ),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xFFD8D8D8),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    SecPrimaryButton(
                      color: Color(0xFF4361BA),
                      text: 'Save',
                      isLoading: false,
                      onTap: () {
                        //update agent profile
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class CustomInput extends StatefulWidget {
  final String label;
  final TextEditingController controller;

  CustomInput({
    this.label,
    this.controller,
  });
  @override
  _CustomInputState createState() => _CustomInputState();
}

class _CustomInputState extends State<CustomInput> {
  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }

  bool visibility = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(
          bottom: ScreenUtil().setHeight(30.5).toDouble(),
        ),
        hintText: widget.label,
        hintStyle: TextStyle(
          fontFamily: 'Rubik',
          fontSize: ScreenUtil().setSp(14).toDouble(),
          color: Color(0xFFB8BCC7),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
      ),
    );
  }
}
