import 'package:flutter/material.dart';
import 'package:security_company_app/app/settings/screens/home/views/customer_settings.dart';
import 'package:security_company_app/app/settings/screens/home/views/provider_seetings.dart';

import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatefulWidget {
  final PageController pageController;
  SettingsScreen({Key key, @required this.pageController}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  String role;

  @override
  void initState() {
    getUserRole();
    super.initState();
  }

  //retrive user role from local strorage
  void getUserRole() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print(prefs.getString('token'));
    setState(() {
      role = prefs.getString('role');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //show settings based on user role
      body: role == 'agent'
          ? ProviderSettings()
          : CustomerSettings(
              pageController: PageController(),
            ),
    );
  }
}
