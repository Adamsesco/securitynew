import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/signup/data/signup_repository.dart';
import './bloc.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  SignUpRepository signUpRepository;

  SignUpBloc({@required this.signUpRepository});

  @override
  SignUpState get initialState => InitialSignUpState();

  @override
  Stream<SignUpState> mapEventToState(
    SignUpEvent event,
  ) async* {
    if (event is SignUpRequested) {
      yield SignUpInProgress();

      final result = await signUpRepository.signUp(
          event.email, event.password, event.firstName, event.lastName);
      if (result == null) {
        yield SignUpFailure(
            errorCode: 2,
            errorMessage: 'An error occurred. Please try again.');
      } else {
        yield SignUpSuccess(user: result);
      }
    }
  }
}
