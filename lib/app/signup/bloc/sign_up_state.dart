import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/user.dart';

abstract class SignUpState extends Equatable {
  const SignUpState();
}

class InitialSignUpState extends SignUpState {
  @override
  List<Object> get props => [];
}

class SignUpInProgress extends SignUpState {
  @override
  List<Object> get props => [];
}

class SignUpFailure extends SignUpState {
  final int errorCode;
  final String errorMessage;

  SignUpFailure({@required this.errorCode, @required this.errorMessage});

  @override
  List<Object> get props => [errorCode, errorMessage];
}

class SignUpSuccess extends SignUpState {
  final User user;

  SignUpSuccess({@required this.user});

  @override
  List<Object> get props => [user];
}
