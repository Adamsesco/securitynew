import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/provider/bottom_app_bar_provider.dart';
import 'package:security_company_app/provider/profile_provider.dart';

class SecurityApp extends StatefulWidget {
  @override
  _SecurityAppState createState() => _SecurityAppState();
}

class _SecurityAppState extends State<SecurityApp> {
  @override
  Widget build(BuildContext context) {
    // ScreenUtil.init( width: 375, height: 812);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => BottomAppBarProvider(),
        ),
                ChangeNotifierProvider(
          create: (_) => ProfileProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Security',
        onGenerateRoute: Router.onGenerateRoute,
        navigatorKey: Router.navigatorKey,
      ),
    );
  }
}
