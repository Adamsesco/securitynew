import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/routes/router.gr.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  int _pageIndex;
  PageController _pageController;
  List<Widget> _pages;

  @override
  void initState() {
    super.initState();

    _pageIndex = 0;
    _pageController = PageController();
    _pages = [
      Container(child: _FirstPage()),
      Container(child: _SecondPage()),
      Container(child: _ThirdPage()),
    ];
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);

      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Palette.sanMarino, Palette.cello],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: Column(
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).padding.top),
              Spacer(flex: 4),
              Expanded(
                flex: 24,
                child: PageView(
                  controller: _pageController,
                  onPageChanged: (int index) {
                    setState(() {
                      _pageIndex = index;
                    });
                  },
                  children: _pages,
                ),
              ),
              Spacer(flex: 2),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    for (var i = 0; i < _pages.length; i++) ...[
                      Container(
                        width: ScreenUtil().setWidth(11).toDouble(),
                        height: ScreenUtil().setWidth(11).toDouble(),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _pageIndex == i
                              ? Colors.white
                              : Colors.white.withOpacity(0.2),
                        ),
                      ),
                      if (i < _pages.length - 1)
                        SizedBox(width: ScreenUtil().setWidth(8).toDouble()),
                    ],
                  ],
                ),
              ),
              Spacer(flex: 3),
              Center(
                child: InkWell(
                  onTap: () {
                    Router.navigator.pushNamed(Router.signinScreen);
                  },
                  child: SizedBox(
                    width: ScreenUtil().setWidth(198).toDouble(),
                    child: SecPrimaryButton(
                      onTap: () {
                        if (_pageIndex < _pages.length - 1) {
                          _pageController.animateToPage(
                            _pageIndex + 1,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.linear,
                          );
                        } else {
                          Router.navigator
                              .pushReplacementNamed(Router.signinScreen);
                        }
                      },
                      color: Colors.white,
                      text: _pageIndex == _pages.length - 1
                          ? 'Get started'
                          : 'Next',
                      textStyle: theme.introButtonTextStyle,
                    ),
                  ),
                ),
              ),
              Spacer(flex: 4),
            ],
          ),
        ),
      );
    });
  }
}

class _FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);
        return Container(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(36).toDouble(),
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Container(
                  child: Image.asset(
                    'assets/images/be-secure.png',
                  ),
                ),
              ),
              Spacer(),
              Text(
                'Be secure, in a few steps',
                textAlign: TextAlign.center,
                style: theme.introHeadStyle,
              ),
              Spacer(),
              Text(
                'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                style: theme.introTextStyle,
              ),
            ],
          ),
        );
      },
    );
  }
}

class _SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);
        return Container(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(36).toDouble(),
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Container(
                  child: Image.asset(
                    'assets/images/securing-and-monitoring.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              Spacer(),
              Text(
                'Securing and monitoring your sites, easier now.',
                textAlign: TextAlign.center,
                style: theme.introHeadStyle,
              ),
              Spacer(),
              Text(
                'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                style: theme.introTextStyle,
              ),
            ],
          ),
        );
      },
    );
  }
}

class _ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);
        return Container(
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(36).toDouble(),
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Container(
                  child: Image.asset(
                    'assets/images/customize-your-request.png',
                  ),
                ),
              ),
              Spacer(),
              Text(
                'Customize your request according to your needs',
                textAlign: TextAlign.center,
                style: theme.introHeadStyle,
              ),
              Spacer(),
              Text(
                'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                style: theme.introTextStyle,
              ),
            ],
          ),
        );
      },
    );
  }
}
