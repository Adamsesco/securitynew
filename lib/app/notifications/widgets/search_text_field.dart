import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class SearchTextField extends StatelessWidget {
  final void Function(String) onChanged;

  SearchTextField({this.onChanged});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Container(
//          height: ScreenUtil().setHeight(44).toDouble(),
          margin: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(12).toDouble(),
          ),
          padding: EdgeInsetsDirectional.only(
              top: ScreenUtil().setHeight(13).toDouble(),
              end: ScreenUtil().setWidth(14).toDouble(),
              bottom: ScreenUtil().setHeight(13).toDouble(),
              start: ScreenUtil().setWidth(14).toDouble()),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(21),
            color: Colors.white,
          ),
          child: Row(
            children: <Widget>[
              Image.asset('assets/images/search-icon.png'),
              SizedBox(width: ScreenUtil().setWidth(10).toDouble()),
              Expanded(
                child: TextField(
                  onChanged: onChanged,
                  style: theme.searchTextStyle,
                  decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    hintText: 'Search',
                    hintStyle: theme.searchTextStyle,
                    contentPadding: EdgeInsets.zero,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
