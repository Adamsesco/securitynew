import 'package:flutter/material.dart';

class Palette {
  static const mystic = Color(0xFFE4E9EF);
  static const waterloo = Color(0xFF888972);
  static const brightGray = Color(0xFF363E49);
  static const woodSmoke = Color(0xFF08090A);
  static const ebonyClay = Color(0xFF212A37);
  static const springWood = Color(0xFFFBF9F7);
  static const sanMarino = Color(0xFF4361BA);

  static const frenchGray = Color(0xFFB8BCC7);
  static const cello = Color(0xFF213266);
  static const alto = Color(0xFFD8D8D8);
  static const alabaster = Color(0xFFFCFCFC);
  static const dullLavender = Color(0xFF9DB0E6);
  static const athensGray = Color(0xFFF3F2F4);
  static const iron = Color(0xFFD4D8DC);
  static const seashell = Color(0xFFF1F1F1);
  static const mercury = Color(0xFFE5E5E5);
  static const wildSand = Color(0xFFF5F5F5);
  static const periwinkleGray = Color(0xFFC2D1E3);
  static const spindle = Color(0xFFA8B7E3);
  static const silverChalice = Color(0xFFA5A5A5);
  static const dustyGray = Color(0xFF9C9C9C);
  static const mischka = Color(0xFFD0D6DC);
  static const luckyPoint = Color(0xFF152963);
  static const supernova = Color(0xFFFFC500);
  static const crusta = Color(0xFFFF823C);
  static const milanoRed = Color(0xFFC21500);
  static const treePoppy = Color(0xFFFFA51C);
  static const sunshade = Color(0xFFFF952A);
  static const greenHaze = Color(0xFF02A748);
  static const greenHaze2 = Color(0xFF06AA4C);
  static const gallery = Color(0xFFEDEDED);
  static const azure = Color(0xFF3C5BB5);
}
