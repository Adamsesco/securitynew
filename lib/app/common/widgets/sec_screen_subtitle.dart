import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class SecScreenSubtitle extends StatelessWidget {
  final String subtitle;
  final bool small;

  SecScreenSubtitle({@required this.subtitle, this.small = false}) : assert(subtitle != null);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);

      return Container(
        margin: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(16).toDouble(),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              subtitle,
              style: small ? theme.screenSubtitleSmallStyle : theme.screenSubtitleStyle,
            ),
            SizedBox(height: ScreenUtil().setHeight(4).toDouble()),
            Container(
              width: ScreenUtil().setWidth(26).toDouble(),
              height: ScreenUtil().setHeight(2.68).toDouble(),
              decoration: BoxDecoration(
                color: Palette.brightGray,
                borderRadius: BorderRadius.circular(3),
              ),
            ),
          ],
        ),
      );
    });
  }
}
