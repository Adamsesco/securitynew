import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/provider/bottom_app_bar_provider.dart';

class SecNavigationBar extends StatefulWidget {
  final PageController pageController;

  SecNavigationBar({@required this.pageController});

  @override
  _SecNavigationBarState createState() => _SecNavigationBarState();
}

class _SecNavigationBarState extends State<SecNavigationBar> {
  void _navigateToPage(int index, BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    widget.pageController.jumpTo(index.toDouble() * width);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    BottomAppBarProvider bottomAppBarProvider =
        Provider.of<BottomAppBarProvider>(context);

    return Container(
      width: MediaQuery.of(context).size.width,
      height: ScreenUtil().setHeight(78 + 18).toDouble(),
      alignment: Alignment.center,
      padding: EdgeInsets.only(
        top: ScreenUtil().setHeight(18).toDouble(),
      ),
      decoration: BoxDecoration(
//        color: Palette.ebonyClay,
        image: DecorationImage(
          image: AssetImage('assets/images/navbar-background.png'),
          fit: BoxFit.fill,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          InkWell(
            onTap: () {
              bottomAppBarProvider.selectedIndex = 0;
              _navigateToPage(0, context);
              //Router.navigator.pushNamed(Router..notificationsScreen);
            },
            child: Container(
              child: Image.asset(
                'assets/images/notifications-icon2.png',
                width: ScreenUtil().setWidth(23).toDouble(),
                height: ScreenUtil().setWidth(22.96).toDouble(),
                color: bottomAppBarProvider.selectedIndex == 0
                    ? Colors.white
                    : Color(0xffD4D8DC),
              ),
            ),
          ),
          /* InkWell(
            onTap: () {
              bottomAppBarProvider.selectedIndex = 0;
              _navigateToPage(0, context);
            },
            child: Container(
              child: Image.asset(
                'assets/images/orders-icon.png',
                width: ScreenUtil().setWidth(20).toDouble(),
                height: ScreenUtil().setWidth(28).toDouble(),
                color: bottomAppBarProvider.selectedIndex == 0
                    ? Colors.white
                    : Color(0xffD4D8DC),
              ),
            ),
          ),*/

          InkWell(
            onTap: () {
              bottomAppBarProvider.selectedIndex = 1;
              _navigateToPage(1, context);
            },
            child: Transform.translate(
              offset: Offset(0, -10),
              child: Container(
                child: Image.asset(
                  'assets/images/shield-icon.png',
                  width: ScreenUtil().setWidth(34).toDouble(),
                  height: ScreenUtil().setWidth(35).toDouble(),
                ),
              ),
            ),
          ),
          /*InkWell(
            onTap: () {
              bottomAppBarProvider.selectedIndex = 2;
              _navigateToPage(2, context);
              //Router.navigator.pushNamed(Router..notificationsScreen);
            },
            child: Container(
              child: Image.asset(
                'assets/images/notifications-icon2.png',
                width: ScreenUtil().setWidth(23).toDouble(),
                height: ScreenUtil().setWidth(22.96).toDouble(),
                color: bottomAppBarProvider.selectedIndex == 2
                    ? Colors.white
                    : Color(0xffD4D8DC),
              ),
            ),
          ),*/
          InkWell(
            onTap: () {
              bottomAppBarProvider.selectedIndex = 2;
              _navigateToPage(2, context);
              // Router.navigator.pushNamed(Router.settingsScreen);
            },
            child: Container(
              child: Image.asset(
                'assets/images/settings-icon.png',
                width: ScreenUtil().setWidth(24).toDouble(),
                height: ScreenUtil().setWidth(24).toDouble(),
                color: bottomAppBarProvider.selectedIndex == 3
                    ? Colors.white
                    : Color(0xffD4D8DC),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

//class _NavigationBarClipper extends CustomClipper<Path> {
//  @override
//  Path getClip(Size size) {
//    final path = Path();
//
//    path.moveTo(0, 18);
//
//    path.lineTo((size.width - 94) / 2, 18);
//
//    path.arcToPoint(
//      Offset((size.width - 94) / 2 + 8, 18 - 6 / 2),
//      clockwise: false,
//      radius: Radius.circular(26 / 2),
//    );
//
//    path.arcToPoint(
//      Offset((size.width + 94) / 2 - 8, 18 - 6 / 2),
//      radius: Radius.circular((140) / 2),
//    );
//
////    path.moveTo(0, ScreenUtil().setHeight(18).toDouble());
////
////    path.lineTo(ScreenUtil().setWidth(141).toDouble(),
////        ScreenUtil().setHeight(18).toDouble());
////
////    path.arcToPoint(
////      Offset(ScreenUtil().setWidth(141 + (12 * 2 / 3)).toDouble(),
////          ScreenUtil().setHeight(18 - 6 / 2).toDouble()),
////      clockwise: false,
////      radius: Radius.circular(ScreenUtil().setWidth(26 / 2).toDouble()),
////    );
////
////    path.arcToPoint(
////      Offset(ScreenUtil().setWidth(223 + (12 / 3)).toDouble(),
////          ScreenUtil().setHeight(18 - 6 / 2).toDouble()),
////      radius: Radius.circular(ScreenUtil().setWidth((140) / 2).toDouble()),
////    );
////    path.conicTo(
////      size.width / 2,
////      0,
////      ScreenUtil().setWidth(223 + (12 / 3)).toDouble(),
////      ScreenUtil().setHeight(18 - 6 / 2).toDouble(),
////      2,
////    );
//
//    path.arcToPoint(
//      Offset((size.width + 94) / 2, 18),
//      clockwise: false,
//      radius: Radius.circular(26 / 2),
//    );
//
//    path.lineTo(size.width, 18);
//    path.lineTo(size.width, size.height);
//    path.lineTo(0, size.height);
//    path.close();
//
//    return path;
//  }
//
//  @override
//  bool shouldReclip(CustomClipper<Path> oldClipper) {
//    return true;
//  }
//}
