import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class SecTextField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final String hintText;
  final bool autocorrect;
  final bool autofocus;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final TextCapitalization textCapitalization;
  final bool obscureText;
  final void Function(String) onChanged;
  final void Function() onEditingComplete;

  SecTextField({
    this.controller,
    this.focusNode,
    this.hintText,
    this.autocorrect = true,
    this.autofocus = false,
    this.keyboardType,
    this.textInputAction,
    this.textCapitalization = TextCapitalization.none,
    this.obscureText = false,
    this.onChanged,
    this.onEditingComplete,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        final appState = state as AppLoadSuccess;

        return TextField(
          controller: controller,
          focusNode: focusNode,
          autocorrect: autocorrect,
          autofocus: autofocus,
          keyboardType: keyboardType,
          textInputAction: textInputAction,
          textCapitalization: textCapitalization,
          obscureText: obscureText,
          onChanged: onChanged,
          onEditingComplete: onEditingComplete,
          style: appState.theme.textFieldStyle,
          decoration: InputDecoration(
//            contentPadding: EdgeInsets.symmetric(
//              vertical: ScreenUtil().setHeight(4.5).toDouble(),
//              horizontal: ScreenUtil().setWidth(43 - 28.5).toDouble(),
//            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 0.2,
                color: Palette.alto,
              ),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 0.2,
                color: Palette.alto,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 0.2,
                color: Palette.alto,
              ),
            ),
            hintText: hintText,
            hintStyle: appState.theme.hintStyle,
          ),
        );
      },
    );
  }
}
