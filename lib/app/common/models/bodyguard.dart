import 'package:security_company_app/app/common/models/language.dart';
import 'package:security_company_app/app/common/models/user.dart';

class Bodyguard extends User {
  @override
  String id;

  @override
  String name;

  String firstName;

  String lastName;

  @override
  String role;


  @override
  String phoneNumber;

  @override
  String avatarUrl;

  @override
  Language language;

  @override
  bool available;

  @override
  bool notificationsEnabled;

  double rating;

  double price;

  Bodyguard(
      {this.id,
      this.name,
        this.role,
      this.firstName,
      this.lastName,
      this.phoneNumber,
      this.avatarUrl,
      this.language,
      this.available,
      this.notificationsEnabled,
      this.rating,
      this.price});

  @override
  factory Bodyguard.fromMap(Map<String, dynamic> map) {
    return Bodyguard();
  }

  @override
  factory Bodyguard.copyFrom(Bodyguard other) {
    return Bodyguard(
      id: other.id,
      name: other.name,
      firstName: other.firstName,
      lastName: other.lastName,
      phoneNumber: other.phoneNumber,
      avatarUrl: other.avatarUrl,
      language: other.language,
      available: other.available,
      notificationsEnabled: other.notificationsEnabled,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return null;
  }
}
