import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/models/sec_service.dart';

class DoorSupervisionSubService extends SecService {
  @override
  String get description =>
      'At vero eos censes aut odit aut quid percipit aut rerum necessitatibus saepe.';

  @override
  Widget get icon => Image.asset(
        'assets/images/security-guard-icon.png',
        width: ScreenUtil().setWidth(50).toDouble(),
        height: ScreenUtil().setWidth(46).toDouble(),
      );

  @override
  String get name => 'DOOR SUPERVISION';

  @override
  List<SecService> get subServices => [];
}
