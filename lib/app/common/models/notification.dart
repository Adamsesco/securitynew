import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/order.dart';

class SecNotification {
  final String id;
  final SecNotificationType type;
  final String text;
  final Order order;
  final DateTime date;
  bool read;

  SecNotification(
      {@required this.id,
      @required this.type,
      @required this.text,
      @required this.date,
      this.order,
      this.read = false});
}

enum SecNotificationType {
  orderNew,
  orderDone,
  orderDelayed,
  orderCanceled,
  custom,
}
