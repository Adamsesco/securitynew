import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/search_data.dart';
import 'package:security_company_app/app/common/widgets/sec_navigation_bar.dart';
import 'package:security_company_app/app/dashboard/screens/company_dashboard_screen.dart';
import 'package:security_company_app/app/notifications/screens/notifications_screen.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/settings/settings_navigator.dart';

class CustomNavigation extends StatelessWidget {
  final PageController pageController = PageController(initialPage: 1);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);
        return Material(
          child: Stack(
            children: <Widget>[
              PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                controller: pageController,
                itemBuilder: (context, int index) {
                  switch (index) {
                    case 0:
                      return NotificationsScreen();
                    case 1:
                      return CompanyDashboardScreen(
                        //  pageController: pageController,
                      );
                    case 2:
                      return SettingsNavigator(
                        pageController: pageController,
                      );
                    default:
                      return SizedBox();
                  }
                },
              ),
              Positioned(
                bottom: 0,
                child: SecNavigationBar(
                  pageController: pageController,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
