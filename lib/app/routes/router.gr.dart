// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:security_company_app/app/dashboard/screens/company_dashboard_screen.dart';
import 'package:security_company_app/app/home/screens/home_screen.dart';
import 'package:security_company_app/app/settings/screens/home/settings_screen.dart';
import 'package:security_company_app/app/signin/screens/signin_screen.dart';
import 'package:security_company_app/app/signup/screens/signup_screen.dart';
import 'package:security_company_app/app/intro/screens/intro_screen.dart';
import 'package:security_company_app/app/common/models/search_data.dart';

import 'package:security_company_app/app/notifications/screens/notifications_screen.dart';

class Router {
  static const homeScreen = '/';
  static const signinScreen = '/signin-screen';
  static const signupScreen = '/signup-screen';
  static const introScreen = '/intro-screen';
  static const locationScreen = '/location-screen';
  static const selectServiceScreen = '/select-service-screen';
  static const selectDateTimeScreen = '/select-date-time-screen';
  static const guardsNumberScreen = '/guards-number-screen';
  static const searchResultsScreen = '/search-results-screen';
  static const companyDashboardScreen = '/company-dashboard-screen';
  static const settingsScreen = '/settings-screen';
  static const notificationsScreen = '/notifications-screen';
  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<Router>();
  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.homeScreen:
        return MaterialPageRoute(
          builder: (_) => HomeScreen(),
          settings: settings,
        );
      case Router.signinScreen:
        return MaterialPageRoute(
          builder: (_) => SigninScreen(),
          settings: settings,
        );
      case Router.signupScreen:
        return MaterialPageRoute(
          builder: (_) => SignupScreen(),
          settings: settings,
        );
      case Router.introScreen:
        return MaterialPageRoute(
          builder: (_) => IntroScreen(),
          settings: settings,
        );



      case Router.companyDashboardScreen:
        return MaterialPageRoute(
          builder: (_) => CompanyDashboardScreen(),
          settings: settings,
        );
      case Router.settingsScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => SettingsScreen(key: typedArgs),
          settings: settings,
        );
      case Router.notificationsScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => NotificationsScreen(key: typedArgs),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//LocationScreen arguments holder class
class LocationScreenArguments {
  final Key key;
  final SearchData searchData;
  LocationScreenArguments({this.key, @required this.searchData});
}

//SelectServiceScreen arguments holder class
class SelectServiceScreenArguments {
  final Key key;
  final SearchData searchData;
  SelectServiceScreenArguments({this.key, @required this.searchData});
}

//SelectDateTimeScreen arguments holder class
class SelectDateTimeScreenArguments {
  final Key key;
  final SearchData searchData;
  SelectDateTimeScreenArguments({this.key, @required this.searchData});
}

//GuardsNumberScreen arguments holder class
class GuardsNumberScreenArguments {
  final Key key;
  final SearchData searchData;
  GuardsNumberScreenArguments({this.key, @required this.searchData});
}

//SearchResultsScreen arguments holder class
class SearchResultsScreenArguments {
  final Key key;
  final SearchData searchData;
  SearchResultsScreenArguments({this.key, @required this.searchData});
}
