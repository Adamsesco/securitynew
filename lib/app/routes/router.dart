import 'package:auto_route/auto_route_annotations.dart';
import 'package:security_company_app/app/dashboard/screens/company_dashboard_screen.dart';

import 'package:security_company_app/app/home/screens/home_screen.dart';
import 'package:security_company_app/app/intro/screens/intro_screen.dart';
import 'package:security_company_app/app/notifications/screens/notifications_screen.dart';
import 'package:security_company_app/app/settings/screens/home/settings_screen.dart';
import 'package:security_company_app/app/signin/screens/signin_screen.dart';
import 'package:security_company_app/app/signup/screens/signup_screen.dart';

@autoRouter
class $Router {
  @initial
  HomeScreen homeScreen;

  SigninScreen signinScreen;

  SignupScreen signupScreen;

  IntroScreen introScreen;


  CompanyDashboardScreen clientDashboardScreen;

 // CompanyDashboardScreen companyDashboardScreen;

  SettingsScreen settingsScreen;

  NotificationsScreen notificationsScreen;
}
