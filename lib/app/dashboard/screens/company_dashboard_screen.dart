import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/apis/dashboard_services.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/widgets/sec_screen_subtitle.dart';
import 'package:security_company_app/app/models/dahsboard.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/dashboard/widgets/order_card.dart';

class CompanyDashboardScreen extends StatefulWidget {
  @override
  _CompanyDashboardScreenState createState() => _CompanyDashboardScreenState();
}

class _CompanyDashboardScreenState extends State<CompanyDashboardScreen> {
  DateTime _selectedDate;
  List<Order> _orders;
  List<Order> _firstOrders;

  int _expandedIndex;

  List<String> items = ["All","NEW Order","Pending","Placed","Canceled","Completed"];
  bool loading = false;
  bool loading1 = false;
  String completed = "";
  String pending = "";
  String cancelled = "";

  String _statusFilter = "All";

  getOrder() async {
    setState(() {
      loading = true;
    });
    Map<String, dynamic> res =
        (await DashboardServices.get_dashboard_infos()) as Map<String, dynamic>;
    if (!this.mounted) return;
    print(res);

    setState(() {
      _orders = List<Order>.from(res["orders"] as List);
      _firstOrders = List<Order>.from(res["orders"] as List);

      print(_orders);

      completed = res["completed"] as String;
      cancelled = res["cancelled"] as String;
      pending = res["pending"] as String;

      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    _selectedDate = DateTime.now();
    getOrder();
  }

  @override
  Widget build(BuildContext context) {
    Widget loadwid = Center(child: CupertinoActivityIndicator());

    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topRight,
              ),
              color: Palette.mystic,
            ),
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                  Container(
                    alignment: AlignmentDirectional.topEnd,
                    margin: EdgeInsetsDirectional.only(
                      end: ScreenUtil().setWidth(21).toDouble(),
                    ),
                    child: Text(
                      'DASHBOARD',
                      style: theme.screenTitleStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(18).toDouble()),
                  Align(
                    alignment: AlignmentDirectional.topStart,
                    child: SecScreenSubtitle(
                      subtitle:
                          'The data displayed on your dashboard is monthly',
                      small: true,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20.32).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(15).toDouble()),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10).toDouble(),
                            ),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  DateFormat('MMMM').format(_selectedDate),
                                  style: theme.dateScreenDateStyle.copyWith(
                                    fontSize: ScreenUtil().setSp(19).toDouble(),
                                  ),
                                ),
                                SizedBox(
                                    width: ScreenUtil().setWidth(8).toDouble()),
                                Image.asset(
                                    'assets/images/dropdown-button-icon.png'),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: ScreenUtil().setWidth(107).toDouble(),
                              height: ScreenUtil().setWidth(107).toDouble(),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.white,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    completed,
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(36).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Palette.sanMarino,
                                    ),
                                  ),
                                  Text(
                                    'COMPLETED',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Palette.sanMarino,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: ScreenUtil().setWidth(107).toDouble(),
                              height: ScreenUtil().setWidth(107).toDouble(),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                gradient: LinearGradient(
                                  colors: [Palette.crusta, Palette.supernova],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  stops: [0.0, 2.0],
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    pending,
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(36).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    'PENDING',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: ScreenUtil().setWidth(107).toDouble(),
                              height: ScreenUtil().setWidth(107).toDouble(),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                gradient: LinearGradient(
                                  colors: [Palette.milanoRed, Colors.red],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    cancelled,
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(36).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    'CANCELED',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(22).toDouble()),
                  Container(
                    margin: EdgeInsetsDirectional.only(
                      start: ScreenUtil().setWidth(15).toDouble(),
                      end: ScreenUtil().setWidth(17).toDouble(),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Orders',
                          style: TextStyle(
                            fontFamily: 'Rubik',
                            fontSize: ScreenUtil().setSp(20).toDouble(),
                            fontWeight: FontWeight.w700,
                            color: Color(0xFFA8B7E3),
                          ),
                        ),

                        PopupMenuButton<String>(
                            padding: EdgeInsets.all(0),
                            elevation: 12,
                            onSelected: (val) {
                              setState(() {
                                _statusFilter =
                                val as String;

                                if(_statusFilter == "All")
                                  {
                                    _orders = _firstOrders;
                                  }
                                else
                              _orders = _firstOrders.where(
                                        (order) =>
                                    /*_statusFilter == "All" ||*/
                                        order.statusText ==
                                            _statusFilter).toList() ;

                              });



                            },
                            offset: Offset(0, 100),
                            child:  Row(
                              children: <Widget>[
                                Text(
                                  'Sort by',
                                  style: theme.dateScreenDateStyle,
                                ),
                                SizedBox(
                                    width: ScreenUtil().setWidth(7).toDouble()),
                                Image.asset(
                                    'assets/images/dropdown-button-icon.png'),
                              ],
                            ),
                            itemBuilder: (BuildContext context) {
                              return items.map((String choice) {
                                return PopupMenuItem<String>(
                                 // height: ScreenUtil.getInstance().setHeight(44),
                                  value: choice,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      ///  SvgPicture.asset( choice != "حسب المسافة"?"assets/images/loc.svg":"",color: Fonts.col_app,),
                                      Container(
                                        width: 6,
                                      ),
                                      Text(
                                        choice,
                                        style: TextStyle(
                                            fontSize:    ScreenUtil().setSp(12).toDouble()),
                                      )
                                    ],
                                  ),
                                );
                              }).toList();
                            }),
                       /* InkWell(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10).toDouble(),
                            ),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  'Sort by',
                                  style: theme.dateScreenDateStyle,
                                ),
                                SizedBox(
                                    width: ScreenUtil().setWidth(7).toDouble()),
                                Image.asset(
                                    'assets/images/dropdown-button-icon.png'),
                              ],
                            ),
                          ),
                        ),*/
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(13).toDouble()),
                  loading
                      ? Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: loadwid)
                      : Expanded(
                          child: Container(
                              margin: EdgeInsets.only(bottom: 72),
                              child: ListView.separated(
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      ScreenUtil().setWidth(7).toDouble(),
                                ),
                                itemCount: _orders?.length ?? 0,
                                separatorBuilder: (_, __) =>

                                /**
                                 *
                                 * final ordersFiltered = _orders.where(
                                    (order) =>
                                    _statusFilter == "All" ||
                                    order.status ==
                                    _statusFilter);

                                    return ordersFiltered
                                 */
                                SizedBox(
                                height:
                                        ScreenUtil().setHeight(5).toDouble()),
                                itemBuilder: (BuildContext context, int index) {
                                  final order = _orders[index];

                                  return OrderCard(
                                    order: order,
                                    expanded: _expandedIndex == index,
                                    onTap: () {
                                      setState(() {
                                        if (_expandedIndex == index) {
                                          _expandedIndex = null;
                                        } else {
                                          _expandedIndex = index;
                                        }
                                      });
                                    },
                                  );
                                },
                              )),
                        ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
