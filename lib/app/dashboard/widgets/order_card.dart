import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/apis/dashboard_services.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/order_status.dart';
import 'package:security_company_app/app/common/widgets/expandible_container.dart';
import 'package:security_company_app/app/models/dahsboard.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';


class OrderCard extends StatefulWidget {
  final Order order;
   bool expanded;
  final void Function() onTap;
  final void Function(Order order) onAction;

  OrderCard(
      {@required this.order, this.expanded = false, this.onTap, this.onAction})
      : assert(order != null);

  @override
  _OrderCardState createState() => _OrderCardState();
}

class _OrderCardState extends State<OrderCard> {
  Color cardColor;
  Color buttonColor;


  chnge_status_api() async {
   // https://getsecured.app/backend/api/providers/{agent / provider}/changeOrderStatus/{orderid}?token={usertoken}&status={status}



  }
  change_status() async {

    switch (widget.order.order_state) {
      case "3":
        setState((){
          widget.order.textColor= Colors.white;
          cardColor = Palette.treePoppy;
          buttonColor = Palette.sunshade;
          widget.order.statusText = 'Pending';
          widget.order.statusButton= 'PUNCH IN';
          widget.expanded = false;
          widget.order.order_state ="2";
        });

        break;
      case "2":
        setState((){
          widget.order.textColor= Colors.white;
          cardColor = Palette.greenHaze;
          buttonColor = Palette.greenHaze2;
          widget.order.statusText = 'In progress';
          widget.order.statusButton= 'FINISH THIS ORDER';
          widget.expanded = false;
          widget.order.order_state ="1";

        });

        break;
      case "1":
        /*
        cardColor = Palette.greenHaze;
        buttonColor = Palette.greenHaze2;
        widget.order.statusText = 'In progress';
        widget.order.statusButton= 'FINISH THIS ORDER';*/
      setState(() {
        widget.order.textColor= Palette.sanMarino;
        cardColor = Colors.white;
        buttonColor = Palette.gallery;
        widget.order.statusText = 'Completed';
        widget.order.statusButton= 'DONE';
        widget.expanded = false;
        widget.order.order_state ="4";

      });

        break;
      case '4':
        //done
        setState(() {
          widget.expanded = false;
        });

        break;
      case "5":
        /*
        widget.order.textColor= Colors.white;
        cardColor = Palette.milanoRed;
        buttonColor = Palette.milanoRed;
        widget.order.statusText = 'Canceled';
        widget. order.statusButton= 'OK';*/

        break;
      case 'canceled':
      // TODO: Handle this case.
        break;
    }
    DashboardServices.chngstatusorder(widget.order.id, widget.order.order_state);
  }

  /**
   *
   */


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    switch (widget.order.order_state) {
      case "3":
        widget.order.textColor= Colors.white;
        cardColor = Palette.sanMarino;
        buttonColor = Palette.azure;
        widget.order.statusText = 'NEW Order';
        widget.order.statusButton= 'TAKE THIS ORDER';

        break;
      case "2":
        widget.order.textColor= Colors.white;
        cardColor = Palette.treePoppy;
        buttonColor = Palette.sunshade;
        widget.order.statusText = 'Pending';
        widget.order.statusButton= 'PUNCH IN';

        break;
      case "1":
        widget.order.textColor= Colors.white;
        cardColor = Palette.greenHaze;
        buttonColor = Palette.greenHaze2;
        widget.order.statusText = 'In progress';
        widget.order.statusButton= 'FINISH THIS ORDER';

        break;
      case '4':
        widget.order.textColor= Palette.sanMarino;
        cardColor = Colors.white;
        buttonColor = Palette.gallery;
        widget.order.statusText = 'Completed';
        widget.order.statusButton= 'DONE';

        break;
      case "5":
        widget.order.textColor= Colors.white;
        cardColor = Palette.milanoRed;
        buttonColor = Palette.milanoRed;
        widget.order.statusText = 'Canceled';
        widget.order.statusButton= 'OK';

        break;
      case 'canceled':
      // TODO: Handle this case.
        break;
    }
  }

  @override
  Widget build(BuildContext context) {

    TextStyle  orderDetailsTitleStyle = TextStyle(
      fontFamily: 'Rubik',
      fontSize: ScreenUtil().setSp(12).toDouble(),
      fontWeight: FontWeight.w400,
      color: widget.order.textColor,
    );

    TextStyle  orderDetailsContentStyle = TextStyle(
      fontFamily: 'Rubik',
      fontSize: ScreenUtil().setSp(12).toDouble(),
      fontWeight: FontWeight.w700,
      color: widget.order.textColor,
    );



    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);


        Widget footer = InkWell(
          onTap: () {
            change_status();
          },
          child: Container(
            decoration: BoxDecoration(
              color: buttonColor,
            ),
            padding: EdgeInsetsDirectional.only(
              top: ScreenUtil().setHeight(22).toDouble(),
              // end: ScreenUtil().setWidth(21).toDouble(),
              bottom: ScreenUtil().setHeight(20).toDouble(),
              // start: ScreenUtil().setWidth(21).toDouble(),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Text(
                  widget.order.statusButton,
                  style:  TextStyle(
                    fontFamily: 'Rubik',
                    fontSize: ScreenUtil().setSp(21).toDouble(),
                    fontWeight: FontWeight.w700,
                    letterSpacing: -0.3,
                    color: widget.order.textColor,
                  ),
                ),
              ],
            ),
          ),
        );
        return ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: InkWell(
            onTap: widget.onTap,
            child: Container(
              padding: EdgeInsets.symmetric(
              //  horizontal: ScreenUtil().setWidth(14).toDouble(),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: cardColor,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(16).toDouble()),
            Container(
              padding: EdgeInsets.symmetric(
                 horizontal: ScreenUtil().setWidth(14).toDouble(),
              ),child: Row(
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          style: TextStyle(
                            fontFamily: 'Rubik',
                            fontSize: ScreenUtil().setSp(17).toDouble(),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.5,
                            color: widget.order.textColor,
                          ),
                          text: widget.order.statusText +'  ' ,
                          children: [
                            TextSpan(
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: ScreenUtil().setSp(17).toDouble(),
                                fontWeight: FontWeight.w300,
                                letterSpacing: 0.5,
                                color: widget.order.textColor,
                              ),
                              text: widget.order.id,
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Text(
                        DateFormat('dd/MM/y').format(
                            DateTime.parse(widget.order.date_order.toString())),
                        style: TextStyle(
                          fontFamily: 'Rubik',
                          fontSize: ScreenUtil().setSp(15).toDouble(),
                          fontWeight: FontWeight.w300,
                          letterSpacing: 0.44,
                          color: widget.order.textColor,
                        ),
                      ),
                    ],
                  )),
                  SizedBox(height: ScreenUtil().setHeight(14).toDouble()),
                  Visibility(
                    visible: !widget.expanded,
                    maintainSize: true,
                    maintainState: true,
                    maintainAnimation: true,
                    child: Container(
                      width: ScreenUtil().setWidth(56.67).toDouble(),
                      height: ScreenUtil().setHeight(5).toDouble(),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.white.withOpacity(0.44),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(6).toDouble()),
                  ExpandibleContainer(
                    expanded: widget.expanded,
                    curve: Curves.ease,
                    duration: const Duration(milliseconds: 400),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsetsDirectional.only(
//                            top: ScreenUtil().setHeight(3).toDouble(),
                            end: ScreenUtil().setWidth(16).toDouble(),
                            bottom: ScreenUtil().setHeight(27).toDouble(),
                            start: ScreenUtil().setWidth(16).toDouble(),
                          ),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Order type',
                                      style: orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      widget.order.order_service,
                                      style: orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(6).toDouble()),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Order mission',
                                      style: orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      widget.order.order_service,
                                      style: orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(6).toDouble()),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Nº of Guards',
                                      style: orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      widget.order.order_postcode,
                                      style: orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(6).toDouble()),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'From',
                                      style: orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      widget.order.order_start_time.toString() ==
                                              "null"
                                          ? ""
                                          : DateFormat('dd E y').format(
                                              DateTime.parse(
                                                  widget.order.order_start_time))
                                      // '17 Jan 2020',
                                      ,
                                      style: orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(6).toDouble()),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'To',
                                      style: orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      widget.order.order_end_time.toString() == "null"
                                          ? ""
                                          : DateFormat('dd E y').format(
                                              DateTime.parse(
                                                  widget.order.order_end_time)),
                                      style: orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(6).toDouble()),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Address',
                                      style: orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      widget.order.order_address1,
                                      style: orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(4).toDouble()),


                            ],
                          ),
                        ),

                        footer
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
