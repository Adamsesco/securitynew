import 'package:dio/dio.dart';
import 'package:security_company_app/app/apis/rest_services.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/status.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:security_company_app/app/models/dahsboard.dart';

class DashboardServices {
  static RestService rest = new RestService();

  static get_dashboard_infos() async {
    //ffd170ac5f883f3f935ac14164b317d0

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("id");
    var pr_resp = await rest.get('providers/serviceProvider/dashboard/2');

    if (pr_resp['status'] == 'No Internet') {
      print('No Internet');
    } else if (pr_resp['status'] == 'error') {
      print('error');
    } else {
      final sp = pr_resp['res']["data"]['orders'] as List;

      String comp = pr_resp['res']["data"]["completed_orders"].toString() as String;
      String pending = pr_resp['res']["data"]["pending_orders"].toString()  as String;
      String cancelled = pr_resp['res']["data"]["canceld_orders"].toString()  as String;

      List<Order> ords =  sp
          .map((map) => Order(

              invoiceId: map['invoiceId'].toString(),
              target: map['target'].toString(),
              order_quantity: map['order_quantity'].toString(),
              order_address1: map['order_address1'].toString(),
              date_order: map['order_time'].toString(),
              order_address2: map['order_address2'].toString(),
              order_city: map['order_city'].toString(),
              order_end_time: map['order_end_time'].toString(),
              status: map['status'].toString(),
              order_start_time: map['order_start_time'].toString(),
              order_longitude: map['order_longitude'].toString(),
              order_paymentmethod: map['order_paymentmethod'].toString(),
              order_postcode: map['order_postcode'].toString(),
              order_scheduled_time: map['order_scheduled_time'].toString(),
              order_service: map['order_service'].toString(),
              order_service_image: map['order_service_image'].toString(),
              order_state: map['status'].toString(),
              order_totalprice: map['order_totalprice'].toString(),
              id: map['id'].toString(),
              provider: map['provider'].toString(),
              trackid: map['trackid'].toString()))
          .toList();

      return {
        "orders": ords,
        "pending": pending,
        "cancelled": cancelled,
        "completed": comp
      };
    }
  }

  static get_user_infos() async {
    //ffd170ac5f883f3f935ac14164b317d0

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    var pr_resp = await rest.get('customers/profile/$token');
    if (pr_resp['status'] == 'No Internet') {
      print('No Internet');
    } else if (pr_resp['status'] == 'error') {
      print('error');
    } else {
      final userData = pr_resp['res']['data'] as Map;
      return Client(
        id: userData['id'] as String,
        email: userData['email'] as String,
        token: userData['token'] as String,
        firstName: userData['firstname'] as String,
        lastName: userData['lastname'] as String,
        avatarUrl: userData['avatar'] as String,
        notificationsEnabled: true,
      );
    }
  }

  static chngstatusorder(String idorder,String status) async {
    //ffd170ac5f883f3f935ac14164b317d0
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    var pr_resp = await rest.get('providers/agent/changeOrderStatus/$idorder?token=$token&status=$status');

    print(pr_resp);
  }

  static getStatus() async {
    var pr_resp = await rest.get('OrdersStatus');
    if (pr_resp['status'] == 'No Internet') {
      print('No Internet');
    } else if (pr_resp['status'] == 'error') {
      print('error');
    } else {
      print(pr_resp);
      print(pr_resp['res']['data']);
      final userData = pr_resp['res']['data'] as List;
      print(userData);

      final sp = pr_resp['res']['data'] as List;
      return sp
          .map((map) => StatusOrder(
                order_status: map['order_status'] as String,
                order_statusid: map['order_statusid'] as String,
              ))
          .toList();
    }
  }
}
