import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/language.dart';
import 'package:security_company_app/app/common/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';


class SignInRepository {
  final String apiUrl;

  SignInRepository({@required this.apiUrl});


  Future<User> signIn(String email, String password,String role) async {

    final formData = FormData.fromMap({
      'email': email,
      'password': password,
      'role': role,

    });
    final request = await Dio().post<String>(apiUrl, data: formData);

    print(apiUrl);
    if (request == null || request.statusCode != 200 || request.data == null) {
      print('Status code: ${request.statusCode}');
      print('Data: ${request.data}');
      return null;
    }

    final responseJson = jsonDecode(request.data);

    final code = responseJson['code'] as int;
    if (code == null || code != 1) {
      print('Code: $code');
      print('Data: ${request.data}');
      return null;
    }

    final userData = responseJson['data'];
    if (userData == null) {
      print('Data: ${request.data}');
      return null;
    }

    print('User data: $userData');

    final user = Client(
      id: userData['id'] as String,
      email: userData['email'] as String,
      token: userData['token'] as String,
      firstName: userData['firstname'] as String,
      lastName: userData['lastname'] as String,
      avatarUrl: userData['avatar'] as String,
      notificationsEnabled: true,
      language: Language.english,
    );
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", user.token);
    prefs.setString("id",user.id );
    prefs.setString("email", user.email);

    //prefs.setString("id","MA");
    print('ID: ${user.id}');
    print('email: ${user.email}');
    print('token: ${user.token}');

    return user;
  }
}
