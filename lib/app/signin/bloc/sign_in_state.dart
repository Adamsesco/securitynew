import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/user.dart';

abstract class SignInState extends Equatable {
  const SignInState();
}

class InitialSignInState extends SignInState {
  @override
  List<Object> get props => [];
}

class SignInInProgress extends SignInState {
  @override
  List<Object> get props => [];
}

class SignInFailure extends SignInState {
  final int errorCode;
  final String errorMessage;

  SignInFailure({@required this.errorCode, @required this.errorMessage});

  @override
  List<Object> get props => [errorCode, errorMessage];
}

class SignInSuccess extends SignInState {
  final User user;

  SignInSuccess({@required this.user});

  @override
  List<Object> get props => [user];
}
