import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class SignInEvent extends Equatable {
  const SignInEvent();
}

class SignInRequested extends SignInEvent {
  final String email;
  final String password;
  final String role;

  SignInRequested({@required this.email, @required this.password,  @required this.role});

  @override
  List<Object> get props => [email, password, role];
}